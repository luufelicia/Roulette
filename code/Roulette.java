import java.util.Scanner;
public class Roulette{
    public static void main(String[] args)
    {
        RouletteWheel wheel = new RouletteWheel();
        Scanner scan = new Scanner(System.in);

        final int INITIAL_AMOUNT = 1000;
        final int MAX_ROULETTE_NUM = 37;
        final int MIN_ROULETTE_NUM = 0;
        final int AMOUNT_WIN_REWARD = 35;
        boolean isValid = false;
        String userAnswer;
        int userBet;
        int userGuess;
        int winAmount = 0;
        int totalWinAmount = 0;
        int totalLossAmount = 0;
        int lossAmount = 0;
        int userBalance = INITIAL_AMOUNT;
        boolean continueGame = true;

        while (continueGame)
        {
            System.out.println("User, would you like to make a bet (Y/N)?");
            userAnswer = scan.next();
            
            if (userAnswer.equals("Y"))
            {
                System.out.println("How much would you like to bet? You currently have " + userBalance + "$.");
                userBet = scan.nextInt();

                if (userBet > 0 && userBet <= userBalance)
                    isValid = true;
                else
                    isValid = false;
                
                while (!isValid)
                {
                    System.out.println("Bet out of bounds. It has to be more than 0 and less than your current amount.");
                    userBet = scan.nextInt();
                    if (userBet > 0 && userBet <= userBalance)
                    isValid = true;
                }
                
                //resetting isVariable variable for future use
                isValid = false;

                System.out.println("What number would you like to bet on?");
                userGuess = scan.nextInt();

                if (userGuess >= MIN_ROULETTE_NUM && userGuess <= MAX_ROULETTE_NUM)
                    isValid = true;

                while (!isValid)
                {
                    System.out.println("Number has to be between 0 and 37.");
                    userGuess = scan.nextInt();
                    if (userGuess >= MIN_ROULETTE_NUM && userGuess <= MAX_ROULETTE_NUM)
                        isValid = true;
                }

                wheel.spin();

                if (wheel.checkUserWin(userGuess))
                {
                    winAmount = userBet * AMOUNT_WIN_REWARD;
                    totalWinAmount += winAmount;
                    userBalance += winAmount;
                    System.out.println("CONGRATULATIONS! You won " + winAmount + "$ and you Balance is now " + userBalance + "$");
                }
                    
                else
                {
                    lossAmount = userBet;
                    totalLossAmount += lossAmount;
                    userBalance -= lossAmount;
                    System.out.println("RIP... You lost " + lossAmount + "$ and you Balance is now " + userBalance + "$");

                }
                    

            }

            else
            {
                System.out.println("Won: " + totalWinAmount);
                System.out.println("Lost: " + totalLossAmount);
                break;
            }
        }

        scan.close();
    }
}