import java.util.Random;
public class RouletteWheel{

    private Random rand;
    private int number = 0;

    public RouletteWheel(){
        rand = new Random();
    }

    public void spin(){
        int upperbound = 38;
        this.number = rand.nextInt(upperbound);
    }

    public int getValue(){
        return this.number;
    }

    public boolean checkUserWin(int userGuess)
    {
        if (this.number == userGuess)
            return true;
        else
            return false;
    }

}